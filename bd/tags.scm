;;; Copyright © 2019 Jakob L. Kreuze <zerodaysfordays@sdf.lonestar.org>
;;; Copyright © 2020 Dimakakos Dimakis <bendersteed@teknik.io>
;;;
;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program. If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (bd tags)
  #:use-module (haunt post)
  #:use-module (srfi srfi-1)
  #:export (group-by-tag
	    count-tags
	    tag-uri))

(define (group-by-tag posts)
  "Given a lisp of haunt posts generate a list grouping tags with the
posts associated with it."
  (let ((table (make-hash-table)))
    (for-each (lambda (post)
                (let ((tags (post-ref post 'tags)))
                  (for-each (lambda (tag)
                              (let ((current (hash-ref table tag)))
                                (if current
                                    (hash-set! table tag (cons post current))
                                    (hash-set! table tag (list post)))))
                            tags)))
              posts)
    (hash-fold alist-cons '() table)))

(define (count-tags posts)
  "Return a list of tags associated with their count in descending
order."
  (sort (map (lambda (tag)
               (list (car tag) (length (cdr tag))))
             (group-by-tag posts))
        (lambda (a b) (> (cadr a) (cadr b)))))

(define (tag-uri tag)
  "Given a TAG return the page that contains only posts associated
with that TAG."
  (string-append "/posts/tag/" tag ".html"))


