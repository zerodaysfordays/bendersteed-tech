;;; Copyright © 2020 Dimakakos Dimakis <bendersteed@teknik.io>
;;;
;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program. If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (bd tag-pages)
  #:use-module (bd tags)
  #:use-module (bd theme)
  #:use-module (haunt html)
  #:use-module (haunt post)
  #:use-module (haunt page)
  #:use-module (haunt utils)
  #:use-module (ice-9 match)
  #:export (tags->page))

(define (tags->page site posts)
  (flat-map (match-lambda
	      ((tag . posts)
	       (make-page (tag-uri tag)
			  (base-template site (tags-template site posts #:title tag)
				     #:title tag)
			  sxml->html)))
	    (group-by-tag posts)))




