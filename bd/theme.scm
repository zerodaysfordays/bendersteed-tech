;;; Copyright © 2020 Dimakakos Dimakis <bendersteed@teknik.io>
;;;
;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program. If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (bd theme)
  #:use-module (bd utils)
  #:use-module (bd tags)
  #:use-module (ice-9 match)
  #:use-module (haunt site)
  #:use-module (haunt post)
  #:use-module (haunt utils)
  #:use-module (haunt builder blog)
  #:export (bendersteed-haunt-theme
	    base-template
	    tags-template))

(define navbar
  '(nav (@ (class "container nav"))
    (a (@ (href "https://bendersteed.tech")
	  (class "brand"))
       (img (@ (src "/assets/img/logo.png"))))
    (input (@ (id "bmenub")
	      (type "checkbox")
	      (class "show")))
    (label (@ (for "bmenub")
	      (class "burger pseudo button"))
	   ☰)
    (div (@ (class "menu"))
	 (a (@ (href "/#contact")) "contact")
	 (a (@ (href "/posts")) "posts")
	 (a (@ (href "/#projects")) "projects")
	 (a (@ (href "/resume.html")) "resume")
	 (a (@ (href "https://poems.bendersteed.tech")) "poems"))))

(define footer
  `(footer (@ (class "container footer"))
    (div (@ (class "copyright flex two"))
	 (div (p "© bendersteed, 2020")
	      (p "All content is shared under the
Creative Commons Attribution-Sharealike 4.0 International license."))
	 (div (p "This is site is built using org-mode and haunt. The complete source can be found "
		 (a (@ (href "https://gitlab.com/bendersteed/bendersteed-tech")) "here") ".")
	      (p "Version 0.5.2")))))

(define back-buttons
  `(div (@ (class "buttons-container"))
	 (a (@ (class "button")
	       (href "/"))
	    (i (@ (class "fas fa-angle-double-left"))) "Back to homepage")
	 (a (@ (class "button")
	       (href "/posts"))
	    (i (@ (class "fas fa-angle-left"))) "Back to posts")))

(define* (base-template site body #:key title)
  `((doctype html)
    (head
     (meta (@ (charset "utf-8")))
     (meta (@ (name "viewport")
	      (content "width=device-width, initial-scale=1")))
     (title ,(if title
		 (string-append title " | bendersteed.tech")
		 (site-title site)))
     ;; css
     (link (@ (rel "stylesheet")
	      (href "https://unpkg.com/picnic")))
     (link (@ (rel "stylesheet")
	      (href "/assets/css/all.min.css")))
     (link (@ (rel "stylesheet")
	      (href "/assets/css/bd.css")))
     (link (@ (rel "stylesheet")
	      (href "/assets/css/syntax.css")))
     
     ;; atom feed
     (link (@ (rel "alternate")
	      (title "bendersteed.tech")
	      (type "application/atom+xml")
	      (href "/feed.xml")))
     (link (@ (rel "shortcut icon")
	      (type "image/ico")
	      (href "/assets/img/favicon.ico"))))
    (script (@ (src "https://polyfill.io/v3/polyfill.min.js?features=es6")))
    (script (@ (id "MathJax-script")
               (async "")
               (src "https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js")))
    (body (@ (class ""))
	  ,navbar
	  ,body
	  ,footer)))

(define (post-header site post)
  `(div (@ (class "post"))
	(div (@ (class "title"))
	     (h2 (a (@ (href ,(post-uri site post)))
		    ,(post-ref post 'title))))
	(div (@ (class "subtitle"))
	     (p "posted at " ,(date->string* (post-date post))
		", tagged with " (span (@ (class "tags"))
				       ,(map (lambda (tag)
					       `(span
						 (a (@ (href ,(tag-uri tag))
						       (class "label warning")) ,tag) " "))
					     (post-ref post 'tags)))))))

(define (post-template post)
  `((section (@ (id "post"))
	     (div (@ (class "container"))
		  (div (@ (class "title"))
		       (h1 ,(post-ref post 'title)))
		  (div (@ (class "subtitle"))
		       (p "Posted at " ,(date->string* (post-date post))
			  ", tagged with " (span (@ (class "tags"))
						 ,(map (lambda (tag)
							 `(span
							   (a (@ (href ,(tag-uri tag))
								 (class "label warning")) ,tag) " "))
						       (post-ref post 'tags)))))
		  (div (@ (class "post-content"))
		       (div (@ (class "is-size-5"))
			    ,(post-sxml post)))))
    ,back-buttons))

(define (collection-template site title posts prefix)
  `((section (@ (id "posts"))
	     (div (@ (class "container"))
		  (h1 "all posts "
		      (span (a (@ (href "/feed.xml"))
			      (i (@ (class "fas fa-rss"))))))
		  (div (@ (class "post-listing"))
		       ,(map (lambda (post) (post-header site post))
			     (posts/reverse-chronological posts)))))
    (section (@ (id "tag-cloud"))
	     (div (@ (class "container"))
		  (h1 "tag cloud")
		  (div (@ (class "flex tags"))
		       ,(map (match-lambda
			       ((tag count)
				`(a (@ (class "label warning tag")
				       (href ,(tag-uri tag)))
				    ,tag ": " ,count)))
			     (count-tags posts)))))
    (div (@ (class "buttons-container"))
	 (a (@ (class "button")
	       (href "/"))
	    (i (@ (class "fas fa-angle-left"))) "Back to homepage"))))

(define* (tags-template site posts #:key title)
  `((section (@ (id "posts"))
	     (div (@ (class "container"))
		  (h1 "tagged with #" ,title)
		  (div (@ (class "post-listing"))
		   ,(map (lambda (post)
			   (post-header site post))
			 (posts/reverse-chronological posts)))))
    ,back-buttons))

(define bendersteed-haunt-theme
  (theme #:name "bendersteed.tech"
	 #:layout
	 (lambda (site title body)
	   (base-template
	    site body
	    #:title title))
	 #:post-template post-template
	 #:collection-template collection-template))
