;;; Copyright © 2020 Dimakakos Dimakis <bendersteed@teknik.io>
;;;
;;; This program is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program. If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (bd static-pages)
  #:use-module (bd utils)
  #:use-module (bd theme)
  #:use-module (bd tags)
  #:use-module (haunt builder blog)
  #:use-module (haunt post)
  #:use-module (haunt page)
  #:use-module (haunt utils)
  #:use-module (haunt html)
  #:export (index-page
	    resume-page))

;; index page

(define about
  `(section (@ (id "about"))
	    (div (@ (class "container"))
		 (h1 "hi there, welcome to my space in the web.")
		 (p "I've used the handle bendersteed for over a
decade now, exploring the depths of computer networks. My IRL name is
Dimos (/ˈði.mos/). I'm a mathematician and programmer, a supporter of
Free Software since I first owned a computer. You can have a look at
my resume "
		    (a (@ (href "/resume.html")) "here") ".")
		 (p "Apart from programming and math, I'm really into
DIY, amateur radio, hiking, yoga, martial arts, philosophy and poetry.")
		 (p "This is a place for me to vent off and share my "
		    (a (@ (href "#projects")) "projects") ", views on "
		    (a (@ (href "#blog")) "technology and every-day life") " and the occasional "
		    (a (@ (href "https://poems.bendersteed.tech")) "poem") "."))))

(define contact
  `(section (@ (id "contact"))
	    (div (@ (class "container"))
		 (h1 "contact")
		 (p "Below I list my main ways of socializing in the
wired. Don't hesitate to contact me for any reason -- except spam of course.")
		 (div (@ (class "social flex"))
		      (div (i (@ (class "far fa-envelope")))
			   " "
			   (a (@ (href "mailto:me@bendersteed.tech")) "me at bendersteed.tech"))
		      (div (i (@ (class "fas fa-key")))
			   " "
			   (a (@ (href "/assets/pubkey.txt")) "PGP key"))
		      (div (i (@ (class "fab fa-microblog")))
			   " "
			   (a (@ (href "https://pleroma.soykaf.com/bendersteed")) "pleroma"))
		      (div (i (@ (class "fab fa-gitlab")))
			   " "
			   (a (@ (href "https://gitlab.com/bendersteed")) "gitlab"))))))

(define (blog-section site posts)
  `(section (@ (id "blog"))
	    (div (@ (class "container "))
		 (h1 "recent posts "
		     (span (a (@ (href "/feed.xml"))
			      (i (@ (class "fas fa-rss"))))))
		 (div (@ (class "post-listing"))
		      ,(map (lambda (post)
			      `(div (@ (class "post"))
				    (div (@ (class "title"))
					 (h2 (a (@ (href ,(post-uri site post)))
						,(post-ref post 'title))))
				    (div (@ (class "subtitle"))
					 (p "Posted at " ,(date->string* (post-date post))
					    ", tagged with " (span (@ (class "tags"))
								   ,(map (lambda (tag)
									   `(span
									     (a (@ (href ,(tag-uri tag))
										   (class "label warning")) ,tag) " "))
									 (post-ref post 'tags)))))))
			    (take-up-to 10 (posts/reverse-chronological posts))))
		 (div (a (@ (class "button center")
			    (href "/posts"))
			 (i (@ (class "fas fa-angle-right"))) "View all posts")))))

(define projects
  `(section (@ (id "projects"))
	    (div (@ (class "container "))
		 (h1 "projects")
		 (p "Here you can find some of my projects and
contributions I deem worthy of sharing:")
		 (div (@ (class "flex projects"))
		      (article (@ (class "card project"))
			       (header
				(h2 (a (@ (href "https://gitlab.com/bendersteed/pleroma-bot"))
					      "Pleroma Bot")))
			       (footer
				(p "A very simple bot for activitypub servers in Common Lisp.")))
		      (article (@ (class "card project"))
			       (header
				(h2 (a (@ (href "https://gitlab.com/bendersteed/hate"))
					      "Hate")))
			       (footer
				(p "A toy, full stack Clojure,
app. Vote against things and people you hate.")))
		      (article (@ (class "card project"))
			       (header
				(h2 (a (@ (href "https://gitlab.com/bendersteed/heroku-buildpack-common-lisp"))
					      "Heroku Buildpack for Common Lisp")))
			       (footer
				(p "A fork of Duncan Bayne's buildpack that supports SBCL.")))
		      (article (@ (class "card project"))
			       (header
				 (h2 (a (@ (href "https://git.sr.ht/~jakob/ox-haunt"))
					      "ox-haunt")))
			       (footer (p "An org-mode exporter for
Haunt. I contributed the one-page per org heading workflow and I use
it to write articles for this blog.")))
		      (article (@ (class "card project"))
			       (img (@ (src "/assets/img/gol.jpg")))
			       (header (h2 (a (@ (href "https://onehtmlpagechallenge.com/entries/game_of_life.html"))
					      "Game of Life")))
			       (footer
				(p "A plain implementation of
Conway's Game of Life, in javascript. Single page html, without the
use of any external libraries." )
				(a (@ (href "https://github.com/bendersteed/one-html-page-challenge/blob/master/entries/game_of_life.html")) "source code")))
		      (article (@ (class "card project"))
			       (img (@ (src "/assets/img/tct.jpg")))
			       (header
				(h2 (a (@ (href "https://theconspiracyclub.gr"))
					      "The Conspiracy Club")))
			       (footer (p "A podcast about conspiracies and mysteries, in greek.")
				       (a (@ (href "https://gitlab.com/bendersteed/the-conspiracy-club")) "source code")))
                      (article (@ (class "card project"))
			       (img (@ (src "/assets/img/discordia-chan.jpg")))
			       (header
				(h2 (a (@ (href "https://git.sr.ht/~bendersteed/discordia-chan-fe"))
					      "Discordia-chan")))
			       (footer (p "Add an image board as
comments in any static website. VueJS front-end and Common Lisp
back-end.")))
                      (article (@ (class "card project"))
			       (header
				 (h2 (a (@ (href "https://git.sr.ht/~bendersteed/gsheets-to-gforms"))
                                        "gsheets-to-gforms")))
			       (footer (p "An app script to upload data in google forms from a google sheet.")))))))

(define (index-content site posts)
  `(,about ,contact ,(blog-section site posts) ,projects))

(define (index-page site posts)
  (make-page "index.html"
	     (base-template site (index-content site posts))
	     sxml->html))

;; resume page

(define resume-content
  `((a (@ (class "button pseudo")
	  (href "/")) "⬲ Back to homepage")
    (section (@ (id "skills"))
	     (div (@ (class "container "))
		  (h1 "Skills")
		  (p (h4 "Languages")
		     "Proficient in Common Lisp, Python, Guile Scheme, Emacs Lisp and
Javascript. Familiarity with Ruby, Racket, Clojure, Java, C.")
		  (p (h4 "Technologies")
		     "Have worked with tools and technologies that include
git, Gnu Emacs, Gnu Guix packaging, Archlinux packaging, web
servers (Apache), heroku, container fronts (Docker), SQL (PostgreSQL) and
NoSQL (MongoDB, CouchDB) databases, static site generators (Jekyll,
Hugo, Haunt), and web design frameworks.")))
    (section (@ (id "contributions"))
	     (div (@ (class "container "))
		  (h1 "Free Software Contributions")
		  (p (h4 "Contributed to")
		     "Gnu Guix, Arch Linux,
heroku-common-lisp-buildtool, docker-android, postinstall, ox-haunt and more.")))
    (section (@ (id "experience"))
	     (div (@ (class "container "))
		  (h1 "Work experience")
		  ((h4 "Math tutor")
		   (span "2012 - present")
		   (p "Provided tutoring, to a variety
of students in high school and undergraduate level Math.")
		   (h4 "Break the Borders")
		   (span "2014 - present")
		   (p "Developed and designed the website of the organisation, hosted at "
		      (a (@ (href "https://breaktheborders.gr")) https://breaktheborders.gr) ".")
		   (p "Developed internal CRM tools to streamline the operations of the organisation.")
		   (h4 "Greek Army - Mandatory Military Service")
		   (span "2018 - 2019")
		   (p "Completed my military service as a Reserve
Officer, was selected as leader of my training year.")
		   (p "Developed internal tools for the simplification
of logistics operations and handling the clearance of
expenditure in the military camp where I was appointed.")
		   (p "Developed a survey tool to gather data on the
efficiency of the training methods."))))
    (section (@ (id "other"))
	     (div (@ (class "container "))
		  (h1 "Other")
		  (p "Native Greek speaker, proficient in English, conversant in German.")
		  (p "Organised Network Security and Programming workshops in Local Youth Centers.")
		  (p "Bachelor of Science in Math from UoA.")
		  (p "Consistently self-taught and an avid reader.")))))

(define (resume-page site posts)
  (make-page "resume.html"
	     (base-template site resume-content)
	     sxml->html))
